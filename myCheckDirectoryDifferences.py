import os, sys
import filecmp
import re
from   distutils import dir_util
import shutil
import myConfig

holderlistA      = []
holderlistM      = []
holderlistD      = []

def compareme(newDir, oldDir):
    dircomp        = filecmp.dircmp(newDir,oldDir)
    # dircomp.report()
    only_in_one    = dircomp.left_only
    missing_in_one = dircomp.right_only
    diff_in_one    = dircomp.diff_files
    dirpath        = os.path.abspath(newDir)
    [holderlistA.append(os.path.abspath( os.path.join(newDir,x) )) for x in only_in_one]
    [holderlistM.append(os.path.abspath( os.path.join(newDir,x) )) for x in diff_in_one]
    [holderlistD.append(os.path.abspath( os.path.join(newDir,x) )) for x in missing_in_one]
    if len(dircomp.common_dirs) > 0:
        for item in dircomp.common_dirs:
            compareme(os.path.abspath(os.path.join(newDir,item)), os.path.abspath(os.path.join(oldDir,item)))
        return holderlistA, holderlistM, holderlistD

def main():
 cfg              = myConfig.Mycfg()
 if cfg.isToUseConfig is True:
   pass                         # Do nothing to do I'll get dirs and file info from cfg file
 elif len(sys.argv) > 3:
   cfg.newdir     = sys.argv[1]
   cfg.olddir     = sys.argv[2]
   cfg.outputfile = sys.argv[3]
 else:
   print ( "Usage: ", sys.argv[0], "newDir oldDir HtmlOutputfile" )
   sys.exit(1)

 source_filesA, source_filesM, source_filesD  = compareme(cfg.newdir,cfg.olddir) 
 if len(source_filesA) > 0: 
   with open(cfg.outputfileA, 'w') as out:
      out.write( cfg.htmlStart )
      out.write( cfg.htmlH1A.format(cfg.newdir) ) 
      for var in source_filesA:
         out.write( cfg.htmlOutRow.format(var) + '\n' )
      out.write( cfg.htmlEnd ) 
 if len(source_filesM) > 0: 
   with open(cfg.outputfileM, 'w') as out:
      out.write( cfg.htmlStart )
      out.write( cfg.htmlH1M.format(cfg.newdir) ) 
      for var in source_filesM:
         out.write( cfg.htmlOutRow.format(var) + '\n' )
      out.write( cfg.htmlEnd ) 
 if len(source_filesD) > 0: 
   with open(cfg.outputfileD, 'w') as out:
      out.write( cfg.htmlStart )
      out.write( cfg.htmlH1D.format(cfg.olddir) ) 
      for var in source_filesD:
         out.write( cfg.htmlOutRow.format(var) + '\n' )
      out.write( cfg.htmlEnd ) 
if __name__ == '__main__':
 main()


