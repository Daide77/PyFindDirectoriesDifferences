

class Mycfg:
   def __init__(self):
      self.isToUseConfig = True                                                     # Boolean set to False if you want to pass dir1 dir2 dir3 as parameters
      self.newdir        = "NewDir"                                                 # Put here the new dir to compare
      self.olddir        = "OldDir"                                                 # Put here the old dir to compare
      self.outputfileA   = "Added_Report.html"                                      # Put here the output html file
      self.outputfileM   = "Modified_Report.html"                                   # Put here the output html file
      self.outputfileD   = "Deleted_Report.html"                                    # Put here the output html file
      self.htmlStart     = """ 
<!DOCTYPE html>
<html>
<body> \n"""
      self.htmlH1A       = """<h1>File non presenti in {0}</h1>\n"""                # Html H1 format for added files
      self.htmlH1M       = """<h1>File modificati in {0}</h1>\n"""                  # Html H1 format for changed files
      self.htmlH1D       = """<h1>File cancellato da {0}</h1>\n"""                  # Html H1 format for changed files
      self.htmlOutRow    = """<a href="file:///{0}">{0}</a><br/>\n"""               # Html row format for differences
      self.htmlEnd       = """ 
</body>
</html> \n"""
